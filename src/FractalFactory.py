from mandelbrot import Mandelbrot
from julia import Julia
from psychedelic import Psychedelic


def makeFractal(file):
    frac = {}

    if file is None:
        file = 'data/elephants.frac'

    f = open(file)
    for line in f:
        line = line.lower()
        line = line.rstrip()
        if ': ' in line:
            o = line.split(': ')
            if o[1].isalpha():
                frac[o[0]] = o[1]
            elif '.' in o[1]:
                frac[o[0]] = float(o[1])
            else:
                frac[o[0]] = int(o[1])

    # if frac['type' or 'pixels' or 'centerx' or 'centery' or 'axislength' or 'iterations'] is None:
    #     raise NotImplementedError("Invalid fractal requested")
    if str(frac['type']).isdigit():
        raise NotImplementedError("Invalid fractal requested")
    if frac['pixels' or 'centerx' or 'centery' or 'axislength' or 'iterations'] is str:
        raise NotImplementedError("Invalid fractal requested")
    if '.' in str(frac['pixels' or 'iterations']):
        raise NotImplementedError("Invalid fractal requested")

    fractalType = frac['type']

    if fractalType == 'mandelbrot':
        fractal = Mandelbrot()
    elif fractalType == 'julia':
        fractal = Julia()
    elif fractalType == 'psychedelic':
        fractal = Psychedelic()
    else:
        raise NotImplementedError("Incorrect fractal type")

    if 'creal' and 'cimag' not in frac:
        fractal.setComplex()
    else:
        fractal.setComplex(frac['creal'], frac['cimag'])

    fractal.setIterations(frac['iterations'])
    fractal.setX(frac['centerx'])
    fractal.setY(frac['centery'])
    fractal.setAxisLength(frac['axislength'])
    fractal.setPixel(frac['pixels'])
    return fractal
