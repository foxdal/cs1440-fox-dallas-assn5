from colour import Color

from Gradient import Gradient


class Default(Gradient):
    def __init__(self, steps):
        start = Color('green')
        pink = Color('pink')
        white = Color('white')
        end = Color('orange')
        self.colors = list(start.range_to(pink, steps))
        self.colors += list(pink.range_to(white, steps))[1:]
        self.colors += list(white.range_to(end, steps))[1:]

    def getColor(self, index):
        return self.colors[index]
