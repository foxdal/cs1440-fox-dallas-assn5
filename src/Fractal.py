class Fractal:
    def __init__(self):
        raise NotImplementedError("This is an abstract class")

    def count(self, z):
        raise NotImplementedError("This is an abstract class")

    def getPixel(self):
        return self.pixel
    def getX(self):
        return self.centerX
    def getY(self):
        return self.centerY
    def getAxisLength(self):
        return self.axisLength
    def getIterations(self):
        return self.iterations


    def setPixel(self, pixel):
        self.pixel = pixel
    def setX(self, x):
        self.centerX = x
    def setY(self, y):
        self.centerY = y
    def setAxisLength(self, axisLength):
        self.axisLength = axisLength
    def setIterations(self, iterations):
        self.iterations = iterations
    def setComplex(self, real=0, imaginary=0):
        self.complex = complex(real, imaginary)
