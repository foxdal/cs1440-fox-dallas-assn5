"""
Dallas Fox
CS 1440 - 001
Assn6 - Fractal Visualizer - Design Patterns
Add new features to an already clean code
06 April 2019
"""

import sys

import FractalFactory
import GradientFactory
from ImagePainter import imageOutput


configFileName = None
if len(sys.argv) > 1:
    configFileName = sys.argv[1]

gradientName = None
if len(sys.argv) > 2:
    gradientName = sys.argv[2]


if len(sys.argv) < 2:
    print("FractalFactory: Creating default fractal ...")
    print("GradientFactory: Creating default color gradient ...")
elif len(sys.argv) < 3:
    print("GradientFactory: Creating default color gradient ...")


# When the FractalFactory's argument is None it returns the default fractal
fractal = FractalFactory.makeFractal(configFileName)

# When the GradientFactory's gtype argument is None it returns the default gradient
gradient = GradientFactory.makeGradient(fractal.iterations, gtype=gradientName)

image = imageOutput(fractal, gradient)
