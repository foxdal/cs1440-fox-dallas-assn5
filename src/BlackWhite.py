from colour import Color

from Gradient import Gradient


class GradBlackWhite(Gradient):
    def __init__(self, steps):
        self.colors = []

        for i in range(steps):
            if i % 2 == 0:
                self.colors.append(Color('black'))
            else:
                self.colors.append(Color('white'))

    def getColor(self, n):
        return self.colors[n]
