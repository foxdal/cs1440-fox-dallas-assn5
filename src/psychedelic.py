from Fractal import Fractal


class Psychedelic(Fractal):
    def __init__(self):
        self.type = 'psychedelic'

    def count(self, z):
        c = (0.007 + 1.023j)  # Complex formula for 'psychedelic' fractals

        for i in range(self.iterations):
            if z == 0:
                z = 0.00000001
            z = (z ** (-10)) + c
            if abs(z) > 2:
                return i  # Return a pixel
        return self.iterations - 1  # Escape the sequence if the
                                    # iteration count exceeds the max
