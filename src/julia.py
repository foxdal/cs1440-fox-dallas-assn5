#!/bin/env python3

from Fractal import Fractal


class Julia(Fractal):
    def __init__(self):
        self.type = 'julia'

    def count(self, z):
        c = self.complex

        for i in range(self.iterations):
            z = z * z + c  # Get z1, z2, ...
            if abs(z) > 2:
                return i  # Return a pixel
        return self.iterations - 1  # Escape the sequence if the
                                    # iteration count exceeds the max
