import sys
from tkinter import Tk, Canvas, PhotoImage, mainloop


def makePicture(fractalName, gradient, window):
    """Paint a Fractal image into the TKinter PhotoImage canvas."""

    # Create a PhotoImage with a specified canvas size
    photo = PhotoImage(width=fractalName.getPixel(), height=fractalName.getPixel())

    # Correlate the boundaries of the PhotoImage object to the complex
    # coordinates of the imaginary plane
    boundaryMin = ((fractalName.getX() - (fractalName.getAxisLength() / 2.0)),
                   (fractalName.getY() - (fractalName.getAxisLength() / 2.0)))

    boundaryMax = ((fractalName.getX() + (fractalName.getAxisLength() / 2.0)),
                   (fractalName.getY() + (fractalName.getAxisLength() / 2.0)))

    # Calculates the scale/ratio of the pixel size
    pixelSize = abs(boundaryMax[0] - boundaryMin[0]) / fractalName.getPixel()

    # Create a canvas
    canvas = Canvas(window, width=fractalName.getPixel(), height=fractalName.getPixel(), bg=gradient.getColor(0))
    canvas.pack()
    canvas.create_image((fractalName.getPixel() / 2, fractalName.getPixel() / 2), image=photo, state="normal")

    for column in range(fractalName.getPixel()):
        for row in range(fractalName.getPixel()):
            x = boundaryMin[0] + column * pixelSize
            y = boundaryMin[1] + row * pixelSize
            color = gradient.getColor(fractalName.count(complex(x, y)))
            photo.put(color, (column, row))
        window.update()  # Display a row of pixels

    return photo


def imageOutput(fractal, gradient):
    # Set up the GUI so that we can display the fractal image on the screen
    window = Tk()

    # Display the image on the screen (canvas)
    image = makePicture(fractal, gradient, window)

    # Saves the fractal as a .png image
    imageName = sys.argv[1]
    imageName = imageName.replace('.frac', '')

    image.write(f"{imageName}.png")
    print(f"Saved image {imageName}.png")

    # Call tkinter.mainloop so the GUI remains open
    mainloop()
