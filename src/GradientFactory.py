from BlackWhite import GradBlackWhite
from ColorVerse import GradColorVerse
from Default import Default


def makeGradient(iterations, gtype):
    if gtype:
        gtype = gtype.lower()

    if gtype is None:
        return Default(iterations)
    if gtype == 'blackwhite':
        return GradBlackWhite(iterations)
    elif gtype == 'colorverse':
        return GradColorVerse(iterations)
    else:
        raise NotImplementedError("Invalid gradient requested")
