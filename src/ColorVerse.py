from colour import Color

from Gradient import Gradient


class GradColorVerse(Gradient):
    def __init__(self, steps):
        start = Color('#FF00FF')
        end = Color('#32CD32')
        self.colors = list(start.range_to(end, steps))

    def getColor(self, n):
        return self.colors[n]
