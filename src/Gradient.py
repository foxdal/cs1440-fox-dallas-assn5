class Gradient:
    def __init__(self):
        raise NotImplementedError("Invalid gradient requested")

    def getColor(self, n):
        raise NotImplementedError("Invalid gradient requested")
