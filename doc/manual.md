# User's Manual

This Fractal Visualizer program is very simple, as well as conveniently easy
to use for all users of all skill levels. Simply open the command prompt for
the program ("Terminal" in Pycharm, at the bottom of the screen"). This
command prompt will be the location that you enter all of your input in order
to make the program run in the first place. The info that must be inputted
must be entered like specified below:

    python src/main.py data/FRACTALNAME COLORGRADIENT

Where FRACTALNAME is the name of one of the fractal files that can be ran
through the program to produce a fractal with the given measurements in the
file. In order to get an idea of what fractal files can be used, simply browse
the data folder located in the repository and select one that interests you!
COLORGRADIENT is, quite simply, the colors that the fractal will be painted
in, with which the user can select from 3 color palettes, listed below:

    colorverse  -  Colors ranging from magenta to lime-green
    blackwhite  -  Only black and white colors (very contrasting)
    default     -  Colors ranging from green, pink, white, and orange
    
Note: In order to use the "default" color gradient, the user must leave the
COLORGRADIENT argument in the command line blank, like so:

    python src/main.py data/FRACTALNAME

The other 2 color gradients can be chosen by entering their names (in all lowercase)
into the COLORGRADIENT argument field

Enjoy the program!